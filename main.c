#include <stdio.h>

#include "dlx.h"
#include "sudoku.h"

int main(int argc, char *argv[])
{
    int i;
    Sudoku s;
    Matrix *m;
    DLXData *d;
    DLXData *space[1024] = {0};

    if (argc != 2)
    {
        printf("%s [sudoku file]\n", argv[0]);
        return 1;
    }
    readFromFile(argv[1], &s);

    m = initDLXSudokuMatrix(s.size);
    encodeSudokuInDLX(&s, m);

    d = sparsify(m);

    search(d, space, 0);

    for (i = 0; i < s.size * s.size; i++)
    {
        if (space[i] != 0)
        {
            int row = space[i]->row;
            s.solution->data[row / s.size] = (row % s.size) + 1;
        }
        else 
        {
            printf("No solution found!\n");
            break;
        }
    }
    printSudoku(&s);

    destroyDLXData(d);
    destroyMatrix(m);
    destroySudoku(&s);

    return 0;
}
