#include "matrix.h"

#ifndef _SUDOKU_H
#define _SUDOKU_H

typedef struct 
{
    Matrix *board;
    Matrix *solution;

    int size;
} Sudoku;

void readFromFile(char*, Sudoku*);

void printSudoku(Sudoku*);

void destroySudoku(Sudoku*);
int regionCell(int dlxRow, int size, int sizeSqrt);

Matrix* initDLXSudokuMatrix(int);
void encodeSudokuInDLX(Sudoku*, Matrix*);

#endif
