#include <stdlib.h>
#include <stdio.h>

#include "matrix.h"

Matrix* initMatrix(int rows, int cols)
{
    int i, j;
    Matrix *m = malloc(sizeof(Matrix));
    if (m != NULL)
    {
        m->data = malloc(rows * cols * sizeof(int));
        if (m->data != NULL)
        {
            m->rows = rows;
            m->cols = cols;
            for (i = 0; i < rows; i++)
            {
                for (j = 0; j < cols; j++)
                {
                    m->data[i * cols + j] = 0;
                }
            }
            return m;
        }
        else
        {
            free(m);
        }
    }
    return NULL;
}

void destroyMatrix(Matrix *m)
{
    if (m != NULL)
    {
        free(m->data);
        free(m);
    }
}

static void printMatrixWithSep(Matrix *m, char sep)
{
    int i, j;

    for (i = 0; i < m->rows; i++)
    {
        for (j = 0; j < m->cols; j++)
        {
            printf("%d%c", m->data[i * m->cols + j], sep);
        }
        printf("\n");
    }
}

void printMatrix(Matrix *m)
{
    printMatrixWithSep(m, ' ');
}

