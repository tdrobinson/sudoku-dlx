#include "matrix.h"

#ifndef _DLX_H
#define _DLX_H

typedef struct _DLX_Data_Object
{
    struct _DLX_Data_Object* left;
    struct _DLX_Data_Object* right;
    struct _DLX_Data_Object* up;
    struct _DLX_Data_Object* down;

    struct _DLX_Data_Object* header;

    int column;
    int row;

    int size;
} DLXData;

int search(DLXData*, DLXData*[], int);
DLXData* chooseColumn(DLXData*);

void coverColumn(DLXData*, DLXData*);
void uncoverColumn(DLXData*, DLXData*);

void printSolution(DLXData*[], int size);

DLXData* sparsify(Matrix*);

void destroyDLXData(DLXData*);
void destroyDLXDataColumn(DLXData*, DLXData*);

#endif
