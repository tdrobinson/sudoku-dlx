#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "sudoku.h"

void readFromFile(char* fileName, Sudoku *sudoku)
{
    FILE *fp;
    int i, j;

    sudoku->size = -1;

    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "Unable to read from file %s\n", fileName);
        return;
    }

    if (!feof(fp) && fscanf(fp, "%d", &sudoku->size) == 1 && sudoku->size > 0)
    {
        sudoku->board = initMatrix(sudoku->size, sudoku->size);
        sudoku->solution = initMatrix(sudoku->size, sudoku->size);
        for (i = 0; i < sudoku->size; i++)
        {
            for (j = 0; j < sudoku->size; j++)
            {
                fscanf(fp, "%d", &sudoku->board->data[i * sudoku->size + j]);
                sudoku->solution->data[i * sudoku->size + j] = sudoku->board->data[i * sudoku->size + j];
            }
        }
    }

    fclose(fp);
}

void printSudoku(Sudoku *sudoku)
{
    if (sudoku->size > 0)
    {
        printMatrix(sudoku->solution);
    }
}

void destroySudoku(Sudoku *sudoku)
{
    if (sudoku->size > 0)
    {
        destroyMatrix(sudoku->board);
        destroyMatrix(sudoku->solution);
    }
}

Matrix* initDLXSudokuMatrix(int sudokuSize)
{
    int i;
    int sudokuSizeSqRooted = (int) pow(sudokuSize, 0.5);
    int sudokuSizeSquared = sudokuSize * sudokuSize;
    int sudokuSizeCubed = sudokuSizeSquared * sudokuSize;
    Matrix *m = initMatrix(sudokuSizeCubed, sudokuSizeSquared * 4);
    int *dlxSudokuMatrix = m->data;

    for (i = 0; i < sudokuSizeCubed; i++)
    {
        dlxSudokuMatrix[i * m->cols + (i / sudokuSize)] = 1;
        dlxSudokuMatrix[i * m->cols + (i / sudokuSizeSquared * sudokuSize) + (i % sudokuSize) + sudokuSizeSquared] = 1;
        dlxSudokuMatrix[i * m->cols + (i % sudokuSizeSquared) + 2 * sudokuSizeSquared] = 1;
        dlxSudokuMatrix[i * m->cols + sudokuSize * regionCell(i, sudokuSize, sudokuSizeSqRooted) + (i % sudokuSize) + 3 * sudokuSizeSquared] = 1;
    }
    return m;
}

int regionCell(int dlxRow, int size, int sizeSqrt)
{
    int cell = dlxRow / size;
    int row = cell / size;
    int col = cell % size;

    int i = 0;
    int x = 0, y = 0;
    for (i = 0; i < sizeSqrt; i++)
    {
        int lower = i * sizeSqrt;
        int upper = (i + 1) * sizeSqrt;
        if (lower <= col && col < upper)
        {
            x = i;
        }
        if (lower <= row && row < upper)
        {
            y = i;
        }
    }
    return y * sizeSqrt + x;
}

void encodeSudokuInDLX(Sudoku *sudoku, Matrix *dlx)
{
    int i, j, k;
    for (i = 0; i < sudoku->size; i++)
    {
        for (j = 0; j < sudoku->size; j++)
        {
            for (k = 0; k < sudoku->size; k++)
            {
                int value = sudoku->board->data[i * sudoku->size + j];
                if (value != 0 && value - 1 != k)
                {
                    int row = i * sudoku->size *sudoku->size + j * sudoku->size + k;
                    dlx->data[row * dlx->cols + (row / sudoku->size)] = 0;   
                }
            }
        }
    }
}
