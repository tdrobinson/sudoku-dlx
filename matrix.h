
#ifndef _MATRIX_H
#define _MATRIX_H

typedef struct
{
    int *data;

    int rows;
    int cols;
} Matrix;

Matrix* initMatrix(int, int);
void destroyMatrix(Matrix*);
void printMatrix(Matrix*);

#endif
