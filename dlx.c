#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "dlx.h"

int search(DLXData *h, DLXData *O[], int k)
{
    DLXData *r, *j, *c;
    int done = 0;

    if (h->right == h)
    {
        return 1;
    }
    
    c = chooseColumn(h);
    coverColumn(h, c);

    for (r = c->down; r != c && !done; r = r->down)
    {
        O[k] = r;
        for (j = r->right; j != r; j = j->right)
        {
            coverColumn(h, j->header);
        }
        done = search(h, O, k+1);
        r = O[k];
        c = r->header;
        for (j = r->left; j != r; j = j->left)
        {
            uncoverColumn(h, j->header);
        }
    }
    uncoverColumn(h, c);
    return done;
}

DLXData *chooseColumn(DLXData *hs)
{
    int minSize = INT_MAX;
    DLXData *minColumn = 0;
    DLXData *h;

    for (h = hs->right; h != hs; h = h->right)
    {
        if (h->size < minSize)
        {
            minSize = h->size;
            minColumn = h;
        }
    }

    return minColumn;
}

void coverColumn(DLXData *hs, DLXData *c)
{
    DLXData *i, *j;
    c->right->left = c->left;
    c->left->right = c->right;

    for (i = c->down; i != c; i = i->down)
    {
        for (j = i->right; i != j; j = j->right)
        {
            j->down->up = j->up;
            j->up->down = j->down;
            j->header->size = j->header->size - 1;
        }
    }
}

void uncoverColumn(DLXData *hs, DLXData *c)
{
    DLXData *i, *j;
    for (i = c->up; i != c; i = i->up)
    {
        for (j = i->left; j != i; j = j->left)
        {
            j->header->size = j->header->size + 1;
            j->down->up = j;
            j->up->down = j;
        }
    }
    c->right->left = c;
    c->left->right = c;
}

void printSolution(DLXData *solution[], int size)
{
    int i;

    printf("solution found:\n");
    for (i = 0; i < size; i++)
    {
        printf("%d ", solution[i]->row);
    }
    printf("\n");
}

DLXData* sparsify(Matrix *m)
{
    int i, j;
    DLXData *h = malloc(sizeof(DLXData));
    DLXData **lowest = malloc(m->cols * sizeof(DLXData*));

    DLXData *first, *newH, *last = h;

    h->up = h;
    h->down = h;

    for (i = 0; i < m->cols; i++)
    {
        newH = malloc(sizeof(DLXData));
        last->right = newH;
        newH->left = last;
        newH->right = NULL;
        newH->up = newH;
        newH->down = newH;
        newH->header = newH;
        newH->size = 0;
        newH->column = i;
        newH->row = -1;
        lowest[i] = newH;
        last = newH;
    }
    last->right = h;
    h->left = last;

    for (i = 0; i < m->rows; i++)
    {
        last = NULL;
        first = NULL;
        for (j = 0; j < m->cols; j++)
        {
            if (m->data[i * m->cols + j] == 1)
            {
                DLXData *newData = malloc(sizeof(DLXData));
                newData->up = lowest[j];
                lowest[j]->down = newData;
                lowest[j]->header->size = lowest[j]->header->size + 1;
                newData->header = lowest[j]->header;
                newData->row = i;
                newData->column = j;
                if (last != NULL)
                {
                    newData->left = last;
                    last->right = newData;
                }
                if (first == NULL)
                {
                    first = newData;
                }
                lowest[j] = newData;
                last = newData;
            }
        }
        last->right = first;
        first->left = last;
    }

    for (i = 0; i < m->cols; i++)
    {
        lowest[i]->down = lowest[i]->header;
        lowest[i]->header->up = lowest[i];
    }

    free(lowest);
    return h;
}

void destroyDLXData(DLXData *h)
{
    DLXData *i = h->right;
    while (i != h)
    {
        DLXData *j = i->right;
        destroyDLXDataColumn(i, i->down);
        free(i);
        i = j;
    }
    free(h);
}

void destroyDLXDataColumn(DLXData *h, DLXData *n)
{
    if (h != n)
    {
        DLXData *next = n->down;
        free(n);
        destroyDLXDataColumn(h, next);
    }
}
